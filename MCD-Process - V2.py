
# coding: utf-8

# In[52]:


import pandas as pd
import requests
import ssl
import shutil
import gdal
import os
import pyproj
import numpy as np
import geopandas as gpd
from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon
from shapely.prepared import prep
import sys
import re
import glob

#read the files to download
files=pd.read_csv("rutasMCD19A2-07162018.csv", sep=",")
files.columns=["index","ruta"]
files=files.drop(columns=["index"])
files["names"]=files["ruta"]
files['names']=files["names"].str.split("/",n=-1, expand=True).iloc[:,7]
files[['annday','zona']]=files["names"].str.split(".", expand=True).iloc[:,1:3]
filesCO_CR_ECU=files.loc[files['zona'].str.contains("h10v08|h09v07|h09v08|h10v09", na=False),:]
filesCO_CR_ECU.to_csv("Rutas_CO_CR_ECU.csv",sep="|")
filesCO_CR_ECU["zona"].unique()

testruta=filesCO_CR_ECU.iloc[0,0]
testname=filesCO_CR_ECU.iloc[0,1]

#function to download hdf files guiven the ruta and the filename.
def downloadhdf(ruta,filename):
    try:
        filename="MDCfiles/"+filename
        url='https://ladsweb.modaps.eosdis.nasa.gov'+ruta
        headers = {}
        headers['key'] = 'xxxxxxxxxxxxxxxxxxx' # register an app to get your key: https://wiki.earthdata.nasa.gov/display/EL/How+To+Use+Tokens+From+Another+Earthdata+Login+Application
        #CTX = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
        resp = requests.get(url,headers=headers,verify=True,stream=True)
        resp.raw.decode_content = True
        with open(filename, 'wb') as f:
            shutil.copyfileobj(resp.raw, f)
    except Exception as e:
        print (e.message, e.args) 


CTX = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
#downloadhdf(testruta,testname)... I already downloaded what we needed
'''for index,row in filesCO_CR_ECU.iterrows():
    #print(row['ruta'])
    #print(row['names'])
    downloadhdf(row['ruta'],row['names'])'''


crs = {'init': 'epsg:4326'}
df= pd.read_csv('cities.csv')
geometry = [Point(xy) for xy in zip(df.x, df.y)]
cities = gpd.GeoDataFrame(df, crs=crs, geometry=geometry)
cities['geometry']  = cities.geometry.buffer(0.450)

for FILE_NAME in glob.iglob('MDCfiles/*.hdf', recursive=True):
    print(FILE_NAME)
    #"FILE_NAME= 'C:\python data\MCD19A2.A2005291.h10v08.006.2018034105834.hdf'
    name=re.search("MCD19A2.*[v][0-9][0-9]",FILE_NAME)
    name=name.group()
    print(name)
    DATAFIELD_NAME ='Optical_Depth_047'
    GRID_NAME = 'grid1km'
    gname = 'HDF4_EOS:EOS_GRID:"{0}":{1}:{2}'.format(FILE_NAME,GRID_NAME,DATAFIELD_NAME)
    gdset = gdal.Open(gname)
    data_pc = gdset.ReadAsArray()
    meta = gdset.GetMetadata()
    print(meta)
    n_orbits = meta['Orbit_amount']
    #ending_time = meta['GRANULEENDINGDATETIME']
    #ending_time =ending_time.split(",")
    begin_time = meta['Orbit_time_stamp']

    begin_time =begin_time.split()
    #print(begin_time)

    date=meta['RANGEENDINGDATE']
    #print(ending_time)
    n_orbits=int(n_orbits)
    # Construct the grid.
    x0, xinc, _, y0, _, yinc = gdset.GetGeoTransform()
    nx, ny = (gdset.RasterXSize, gdset.RasterYSize)
    x = np.linspace(x0, x0 + xinc*nx, nx)
    y = np.linspace(y0, y0 + yinc*ny, ny)
    xv, yv = np.meshgrid(x, y)
    sinu = pyproj.Proj("+proj=sinu +R=6371007.181 +nadgrids=@null +wktext")
    wgs84 = pyproj.Proj("+init=EPSG:4326")
    # Convert the grid back to lat/lons.
    lon, lat= pyproj.transform(sinu, wgs84, xv, yv)


    #obtain the first layer from the 3d hdf
    datafin=pd.DataFrame()
    for i in range(n_orbits):
        print(i)
        if n_orbits==1:
            data=data_pc[0:nx,0:ny].astype(np.int16)
        else:
            data=data_pc[i,0:nx,0:ny].astype(np.int16)

        #print(lon.shape)
        #print(lat.shape)
        print(data.shape)
        #ending_time2=ending_time[i]
        begin_time2=begin_time[i]

        print(begin_time, date)
        lon=lon.reshape(-1, 1)
        lat=lat.reshape(-1, 1)
        data=data.reshape(-1, 1)
        #print(lon.shape)
        #print(lat.shape)
        #print(data.shape)
        dataset = pd.DataFrame({'lon':lon[:,0],'lat':lat[:,0],'data':data[:,0],'hora_ini': begin_time2, 'fecha':date})
        del data
        dataset= dataset[dataset.data != -28672]
        dataset.loc[:,'data'] *= 0.001
        geometry = [Point(xy) for xy in zip(dataset.lon, dataset.lat)]
        #dataset = dataset.drop(['lon', 'lat'], axis=1) keep the coordinates from the aod points
        dataset = gpd.GeoDataFrame(dataset, crs=crs, geometry=geometry)


        #distance nearly 50km

        dataset=gpd.sjoin(dataset,cities,  how='inner')
        dataset.dropna(inplace=True)
        print(dataset.size)
        if dataset.size>0:
            dataset.to_csv('results2/'+name+'_'+str(i)+'aod.csv')
            del dataset
        else:
            print(name+'_'+str(i)+' vacio')



