
#
import pandas as pd
import requests
import ssl
import shutil
import gdal
import os
import pyproj
import numpy as np
import geopandas as gpd
from shapely.geometry import Point, Polygon, MultiPoint, MultiPolygon
from shapely.prepared import prep
import sys
import re

#read list of files created with scrape
files=pd.read_csv("rutasMCD19A2-07162018.csv", sep=",") 

#following lines will create columns to filter/select files of interest 
files.columns=["index","ruta"]
files=files.drop(columns=["index"])
files["names"]=files["ruta"]
files['names']=files["names"].str.split("/",n=-1, expand=True).iloc[:,7]
files[['annday','zona']]=files["names"].str.split(".", expand=True).iloc[:,1:3]

# select the files of interest
filesCO_CR_ECU=files.loc[files['zona'].str.contains("h10v08|h09v07|h09v08|h10v09", na=False),:] 

# save list of files of interest for future analysis 
filesCO_CR_ECU.to_csv("Rutas_CO_CR_ECU.csv",sep="|") 

# check everything is correct
len(filesCO_CR_ECU)
filesCO_CR_ECU.head()

#define function that will download files given a target url ruta and file name.
def downloadhdf(ruta,filename):
    try:
        filename="MDCfiles/"+filename
        url='https://ladsweb.modaps.eosdis.nasa.gov'+ruta
        headers = {}
        headers['key'] = 'xxxxxxxx' # register an app to get your key: https://wiki.earthdata.nasa.gov/display/EL/How+To+Use+Tokens+From+Another+Earthdata+Login+Application
        resp = requests.get(url,headers=headers,verify=True,stream=True)
        resp.raw.decode_content = True
        with open(filename, 'wb') as f:
            shutil.copyfileobj(resp.raw, f)
    except Exception as e:
        print (e.message, e.args) 


# download hdf files
CTX = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
for index,row in filesCO_CR_ECU.iterrows():
    print(row['ruta'])
    print(row['names'])
    downloadhdf(row['ruta'],row['names'])

# process data according needs
def mcdprocess(filename):
    ruta="MDCfiles/"+filename
    DATAFIELD_NAME ='Optical_Depth_047'
    GRID_NAME = 'grid1km'
    gname = 'HDF4_EOS:EOS_GRID:"{0}":{1}:{2}'.format(ruta,GRID_NAME,DATAFIELD_NAME)
    gdset = gdal.Open(gname)
    data = gdset.ReadAsArray()
    
    x0, xinc, _, y0, _, yinc = gdset.GetGeoTransform()
    nx, ny = (gdset.RasterXSize, gdset.RasterYSize)
    x = np.linspace(x0, x0 + xinc*nx, nx)
    y = np.linspace(y0, y0 + yinc*ny, ny)
    xv, yv = np.meshgrid(x, y)
    
    tup=(nx,ny)
    if data.shape==tup:
        data=data[0:nx,0:ny].astype(np.int16)
    else:
        data=data[0,0:nx,0:ny].astype(np.int16)
    
    # Convert the grid back to lat/lons.
    sinu = pyproj.Proj("+proj=sinu +R=6371007.181 +nadgrids=@null +wktext")
    wgs84 = pyproj.Proj("+init=EPSG:4326")
    lon, lat= pyproj.transform(sinu, wgs84, xv, yv)
    meta = gdset.GetMetadata()
    #print(meta)
    ending_time = meta['RANGEENDINGTIME']
    begin_time = meta['RANGEBEGINNINGTIME']
    date=meta['RANGEENDINGDATE']
    print(ending_time, begin_time, date)
    lon=lon.reshape(-1, 1)
    lat=lat.reshape(-1, 1)
    data=data.reshape(-1, 1)
    print(lon.shape)
    print(lat.shape)
    print(data.shape)
    
    dataset = pd.DataFrame({'lon':lon[:,0],'lat':lat[:,0],'data':data[:,0],'hora_ini': begin_time, 'fecha':date})
    dataset= dataset[dataset.data != -28672]
    dataset.loc[:,'data'] *= 0.001
    print(dataset.shape)

    geometry = [Point(xy) for xy in zip(dataset.lon, dataset.lat)]
    #dataset = dataset.drop(['lon', 'lat'], axis=1) keep the coordinates from the aod points
    crs = {'init': 'epsg:4326'}
    gdf = gpd.GeoDataFrame(dataset, crs=crs, geometry=geometry)
    
    df= pd.read_csv('cities.csv')
    geometry = [Point(xy) for xy in zip(df.x, df.y)]
    cities = gpd.GeoDataFrame(df, crs=crs, geometry=geometry)
    #distance nearly 50km
    cities['geometry']  = cities.geometry.buffer(0.450)

    intersections=gpd.sjoin(gdf,cities, how='inner')
    intersections.dropna(inplace=True)
    filename2=filename.replace(".hdf",".csv")
    intersections.to_csv('results/'+filename2)
    
    return("ok")

    
files=[f for f in os.listdir("MDCfiles")]
[mcdprocess(f) for f in files]


