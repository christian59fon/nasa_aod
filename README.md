Objective:
Download Aerosol Optical Depth data from NASA https://ladsweb.modaps.eosdis.nasa.gov/ for research purposes.

Background:
AOD data has been collected since year 2000 by MODIS module. 
AOD Data has been collected with other measures in HDF files.
More info: https://ladsweb.modaps.eosdis.nasa.gov/missions-and-measurements/products/maiac/MCD19A2/    

Process:
1. Select the collection of data of interest. For this case, the collection is the 6 and de Module MCD19A2. 

2. Scrape6MCD19A2: Create a list of the files available in the collection. Use scrape to get the urls of the files of interest.

3. MCD-Process: 
        a. Download the files of interest. It can take high volumes of space so think twice of downloading the entire collection.
        b. Read values of files and validate wether the coordinates are inside the polygons of interest. This polygons have been defined to filter the data for the cities      or location of interest and also to reduce the processing time and storage.
        c.if the data in the .hdf file is relevant for the case, it is processed:
            a. Correct the coordinates
            b. filter the data
            c. create a file

Autors: Daniel Martinez DMARTINEZ@IADB.ORG
        Christian Fonseca CHRISTIANFO@IADB.ORG

