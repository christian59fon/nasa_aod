

### Scrape is an example of geting the list of hdf files published for a particular set of data ### 
from bs4 import BeautifulSoup as bs
import requests
import re
import pandas as pd 

# Select the collection and module as the primary url
url='https://ladsweb.modaps.eosdis.nasa.gov/archive/allData/61/MOD04_3K'

# get copy of the html of the url
response=requests.get(url)
html=bs(response.text,'html.parser')
html2=html.find_all('td')

# Get the content of the collection. in this case the folders/years of available data
resp2=[]
resp3=pd.DataFrame(columns=['ruta'])
for link in html2:
    resp=(link.find('a')) ## "a" contain the tags that have the url of each folder
    if resp is not None:
        enlace=re.findall(r'\"(.*?)\"',str(resp))[0]
        resp2.append(enlace)
        resp3=pd.DataFrame(resp2[1:-1],columns=['ruta']) # filter out the unwanted file "Notice" 

# Define the function that will get the urls for days and files
    resp2=[]
    resp3=pd.DataFrame(columns=['ruta'])
    url='https://ladsweb.modaps.eosdis.nasa.gov'+ruta
    response=requests.get(url)
    html=bs(response.text,'html.parser')
    html2=html.find_all('td')
    for link in html2:
        resp=(link.find('a'))
        if resp is not None:
            enlace=re.findall(r'\"(.*?)\"',str(resp))[0]
            resp2.append(enlace)
            resp3=pd.DataFrame(resp2[1:],columns=['ruta'])
    return (resp3)

# test the function
df=getlink(resp3.iloc[0,0])
df

# create dataframe that will content the url of the days
dias=pd.DataFrame(columns=['ruta'])

# get the url of each day for each year
for index,row in resp3.iterrows():
    diay=getlink(row['ruta'])
    dias=dias.append(diay)

# get the url of each .hdf file in the data frame of days 
horas=pd.DataFrame(columns=['ruta'])
for index,row in dias.iterrows():
    horad=getlink(row['ruta'])
    horas=horas.append(horad,ignore_index=True)

# save the files
horas.to_csv("rutas-04262018.csv")
